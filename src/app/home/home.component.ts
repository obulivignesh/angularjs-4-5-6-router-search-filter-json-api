import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private  contacts:  Array<object> = [];

  // public foodItem: FoodItem;
  public searchString: string;


  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getContacts();
  }

  public  getContacts() {
    this.apiService.getContacts().subscribe((data:  Array<object>) => {
        this.contacts  =  data;
        console.log(data);
    });
}

}
